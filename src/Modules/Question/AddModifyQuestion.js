import React, { Component } from 'react';
import { Button, Select, Input, message, Menu, Dropdown } from 'antd';
import { connect } from 'react-redux';
import ButtonGroup from 'antd/lib/button/button-group';
import './AddModifyQuestion.css';
import TextArea from 'antd/lib/input/TextArea';
import fire from '../../fire';
import { UnControlled as CodeMirror } from 'react-codemirror2';
import PictureWall from '../../Common/PictureWall';
import { deleteFile } from '../../Common/util';
require('codemirror/mode/sql/sql');
require('codemirror/mode/shell/shell');
require('codemirror/mode/htmlembedded/htmlembedded');
require('codemirror/mode/javascript/javascript');

const { Option } = Select;



class AddModifyQuestion extends Component {

    state = {
        selectedQA: 'q',
        selectedType: 'quiz',
        selectedCategory: [],
        questions: [],
        answers: [],
        loading: false,
        id: ''
    }

    componentDidMount() {
        const { params } = this.props.match

        if (params.id !== 'add') {
            this.displayData(params.id);
        }
    }

    displayData = (id) => {
        this.setState({
            loading: true
        })
        fire.database().ref('questions/' + id).once('value').then((snapshot) => {
            const data = snapshot.val();

            if (data) {
                this.setState({
                    id: id,
                    selectedType: data.type,
                    selectedCategory: data.tags,
                    questions: data.questionArray,
                    answers: data.answerArray,
                    loading: false
                })
            } else {
                console.log('Redirect to question not found')
            }
        });
    }

    handleTypeChange = (value) => {
        this.setState({
            selectedType: value
        })
    }

    handleCategoryChange = (value) => {
        this.setState({
            selectedCategory: value
        })
    }

    questionsListClicked = () => {
        this.setState({
            selectedQA: 'q'
        })
    }

    answersListClicked = () => {
        this.setState({
            selectedQA: 'a'
        })
    }

    addTitle = () => {
        const selectedField = this.state.selectedQA === 'q' ? 'questions' : 'answers';

        const newItem = this.state[selectedField] ? [...this.state[selectedField], {
            type: 'title',
            value: ''
        }] : [{
            type: 'title',
            value: ''
        }]

        this.setState({
            [selectedField]: newItem
        });
    }

    addDescription = () => {
        const selectedField = this.state.selectedQA === 'q' ? 'questions' : 'answers';

        const newItem = this.state[selectedField] ? [...this.state[selectedField], {
            type: 'description',
            value: ''
        }] : [{
            type: 'description',
            value: ''
        }]

        this.setState({
            [selectedField]: newItem
        });
    }

    addImage = () => {
        const selectedField = this.state.selectedQA === 'q' ? 'questions' : 'answers';

        const newItem = this.state[selectedField] ? [...this.state[selectedField], {
            type: 'image',
            value: ''
        }] : [{
            type: 'image',
            value: ''
        }]

        this.setState({
            [selectedField]: newItem
        });
    }

    addCode = (language) => {
        const selectedField = this.state.selectedQA === 'q' ? 'questions' : 'answers';

        const newItem = this.state[selectedField] ? [...this.state[selectedField], {
            type: 'code',
            language: language,
            value: ''
        }] : [{
            type: 'code',
            language: language,
            value: ''
        }]

        this.setState({
            [selectedField]: newItem
        });
    }

    onChangeImage = (name, imageUrl) => {
        const splitted = name.split('_');
        const type = splitted[0];
        const index = parseInt(splitted[1], 10);

        const newItem = {
            ...this.state[type][index],
            value: imageUrl
        }

        this.setState({
            [type]: [...this.state[type].slice(0, index), newItem, ...this.state[type].slice(index + 1)]
        })
    }

    displayContentItem = (content, index, type) => {
        let item = null;
        const nameIndex = type + '_' + index;
        
        if (content.type === 'title') {
            item = <div className='titleEdit'><Input name={nameIndex} value={this.state[type][index].value} onChange={this.changeFieldValue} size="large" /></div>
        } else if (content.type === 'description') {
            item = <div className='descriptionEdit' >
                <TextArea name={nameIndex} value={this.state[type][index].value} onChange={this.changeFieldValue} autosize={{ minRows: 4, maxRows: 10 }} />
            </div>
        } else if (content.type === 'code') {
            let lang = content.language;

            if(lang === 'html') lang = 'htmlembedded';
            else if(lang === 'java') lang = 'javascript';

            item = <div className='descriptionEdit' >
                <CodeMirror
                    value={this.state[type][index].value}
                    name={nameIndex}
                    options={{
                        mode: content.language === 'java' ? 'javascript' : content.language,
                        theme: 'material',
                        lineNumbers: true
                    }}
                    onChange={(editor, data, value) => this.changeCodeValue(nameIndex, value)}
                />
            </div>
        } else if (content.type === 'image') {
            item = <div className='descriptionEdit'>
                <PictureWall imageUrl={this.state[type][index].value} name={nameIndex} onChangeImage={this.onChangeImage} />
            </div>
        }

        item = <div className='item' key={index}>
            <div className='itemContent'>{item}</div>
            <div className='itemAction'>
                <Button type="default" shape="circle" icon="up" size="small" onClick={() => this.reOrderUpButton(nameIndex)}/>
                <Button type="default" shape="circle" icon="down" size="small" onClick={() => this.reOrderDownButton(nameIndex)}/>
                <Button type="danger" shape="circle" icon="close" size="small" onClick={() => this.reOrderDeleteButton(nameIndex)} /></div>
        </div>

        return item;
    }

    displayContents = (type) => {
        const contents = this.state[type];

        return !contents ? '' : contents.map((content, index) => this.displayContentItem(content, index, type))
    }

    changeCodeValue = (name, value) => {
        const splitted = name.split('_');
        const type = splitted[0];
        const index = parseInt(splitted[1], 10);

        const newItem = {
            ...this.state[type][index],
            value: value
        }

        this.setState({
            [type]: [...this.state[type].slice(0, index), newItem, ...this.state[type].slice(index + 1)]
        })
    }

    changeFieldValue = (e) => {
        const splitted = e.target.name.split('_');
        const type = splitted[0];
        const index = parseInt(splitted[1], 10);

        const newItem = {
            ...this.state[type][index],
            value: e.target.value
        }

        this.setState({
            [type]: [...this.state[type].slice(0, index), newItem, ...this.state[type].slice(index + 1)]
        })
    }

    saveQuestion = () => {
        const questionsRef = fire.database().ref('questions/');

        if (!this.state.id) {
            let newQuestionRef = questionsRef.push();

            newQuestionRef.set({
                type: this.state.selectedType,
                tags: this.state.selectedCategory,
                questionArray: this.state.questions,
                answerArray: this.state.answers,
                correctCount: 0,
                wrongCount: 0
            }, (error) => {
                if (error) {
                    message.error('Question not successfully saved!');
                    console.log(error);
                } else {
                    message.success('Question is successfully saved!');
                    this.props.history.push('/manage-questions');
                }
            });
        } else {
            const questionRef = fire.database().ref(`questions/${this.state.id}`);
            questionRef.update({
                type: this.state.selectedType,
                tags: this.state.selectedCategory,
                questionArray: this.state.questions,
                answerArray: this.state.answers
            }, (error) => {
                if (error) {
                    message.error('Question not successfully updated!');
                    console.log(error);
                } else {
                    message.success('Question is successfully updated!');
                    this.props.history.push('/manage-questions');
                }
            });
        }
    }

    reOrderUpButton = (name) => {
        const splitted = name.split('_');
        const type = splitted[0];
        const index = parseInt(splitted[1], 10);

        if(index <= 0) return;

        const selectedItem = {
            ...this.state[type][index]
        }
        const previousItem = {
            ...this.state[type][index - 1]
        }

        this.setState({
            [type]: [...this.state[type].slice(0, index - 1), selectedItem, previousItem, ...this.state[type].slice(index + 1)]
        })
    }

    reOrderDownButton = (name) => {
        const splitted = name.split('_');
        const type = splitted[0];
        const index = parseInt(splitted[1], 10);

        if(index >= this.state[type].length - 1) return;

        const selectedItem = {
            ...this.state[type][index]
        }
        const nextItem = {
            ...this.state[type][index + 1]
        }

        this.setState({
            [type]: [...this.state[type].slice(0, index), nextItem, selectedItem, ...this.state[type].slice(index + 2)]
        })
    }

    reOrderDeleteButton = (name) => {
        const splitted = name.split('_');
        const type = splitted[0];
        const index = parseInt(splitted[1], 10);

        this.setState({
            [type]: [...this.state[type].slice(0, index), ...this.state[type].slice(index + 1)]
        })

        if(this.state[type][index].type === 'image' && this.state[type][index].value) {
            const link = this.state[type][index].value;

            if(link.indexOf('/o/') > 0 && link.indexOf('?alt') > 0) {
                const startIdx = link.indexOf("/o/");
                const endIdx = link.indexOf("?alt");
                const imgFile = link.substring(startIdx + 3, endIdx);
                
                deleteFile(imgFile);
            }
        }
    }

    render() {
        const menuLanguage = (
            <Menu>
                <Menu.Item key="java" onClick={() => this.addCode('java')}>Java</Menu.Item>
                <Menu.Item key="javascript" onClick={() => this.addCode('javascript')}>JavaScript</Menu.Item>
                <Menu.Item key="htmlembedded" onClick={() => this.addCode('htmlembedded')}>HTML</Menu.Item>
                <Menu.Item key="sql" onClick={() => this.addCode('sql')}>SQL</Menu.Item>
                <Menu.Item key="bash" onClick={() => this.addCode('bash')}>Bash</Menu.Item>
                <Menu.Item key="shell" onClick={() => this.addCode('shell')}>SHELL</Menu.Item>
                <Menu.Item key="XML" onClick={() => this.addCode('xml')}>XML</Menu.Item>
            </Menu>
        );

        return (
            <div className='rootAddModifyQuestion'>
                <div className='typeAndCategoryDiv'>
                    <Select size='large' value={this.state.selectedType} style={{ width: 200 }} onChange={this.handleTypeChange}>
                        {this.props.types.map(type => <Option value={type.id} key={type.id}>{type.label}</Option>)}
                    </Select>
                    <Select mode="multiple" size='large' value={this.state.selectedCategory} style={{ width: 600 }} onChange={this.handleCategoryChange}>
                        {this.props.categories.map(category => <Option key={category.key} value={category.key}>{category.name}</Option>)}
                    </Select>
                    <Button type="primary" onClick={this.saveQuestion}>Save</Button>
                </div>
                <div className={this.state.selectedQA === 'q' ? 'questionListDivSelected' : 'questionListDiv'} onClick={this.questionsListClicked}>
                    <div className='header'>Question</div>
                    <div className='contents'>
                        {this.displayContents('questions')}
                    </div>
                </div>
                <div className={this.state.selectedQA === 'a' ? 'answerListDivSelected' : 'answerListDiv'} onClick={this.answersListClicked}>
                    <div className='header'>Answer</div>
                    <div className='contents'>
                        {this.displayContents('answers')}
                    </div>
                </div>
                <div className='actionsDiv'>
                    <ButtonGroup>
                        <Button type="primary" icon="font-size" size={this.props.isDesktop ? 'large' : 'small'}
                            onClick={this.addTitle}
                        >{this.props.isDesktop ? 'Add Title' : 'Title'}</Button>
                        <Button type="primary" icon="align-left" size={this.props.isDesktop ? 'large' : 'small'}
                            onClick={this.addDescription}
                        >{this.props.isDesktop ? 'Add Description' : 'Description'}</Button>
                        <Button type="primary" icon="picture" size={this.props.isDesktop ? 'large' : 'small'}
                            onClick={this.addImage}
                        >{this.props.isDesktop ? 'Add Image' : 'Image'}</Button>
                        <Dropdown overlay={menuLanguage}><Button type="primary" icon="code" size={this.props.isDesktop ? 'large' : 'small'} >
                            {this.props.isDesktop ? 'Add Code' : 'Code'}</Button></Dropdown>
                    </ButtonGroup>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    types: state.categoryReducer.types,
    categories: state.categoryReducer.categories,
    isDesktop: state.commonReducer.isDesktop
});

export default connect(mapStateToProps, null)(AddModifyQuestion);