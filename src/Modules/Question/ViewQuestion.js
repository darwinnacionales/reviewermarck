import React, { Component } from 'react';
import { connect } from 'react-redux';
import ButtonGroup from 'antd/lib/button/button-group';
import { Button, Modal, InputNumber, Tag } from 'antd';
import './ViewQuestion.css';
import { withRouter } from 'react-router-dom';

import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { findByField } from '../../Common/util';

// const { Option } = Select;

class ViewQuestion extends Component {
    state = {
        currentQuestionIndex: 0,
        isRandom: false,
        answerHidden: true,
        // questions: [],
        selectedType: 'quiz',
        selectedCategories: [],
        jumpModalVisible: false,
        modalIndex: 0
    }

    componentDidMount() {
        this.setState({
            selectedCategories: this.props.selectedCategoriesFromPreQuiz
        });
    }

    displayHideAnswer = () => {
        this.setState({
            answerHidden: !this.state.answerHidden
        })
    }

    handleTypeChange = (value) => {
        this.setState({
            currentQuestionIndex: 0,
            selectedType: value
        });
    }

    handleCategoryChange = (value) => {
        this.setState({
            currentQuestionIndex: 0,
            selectedCategories: value
        });
    }

    nextQuestion = (arrSize) => {
        this.setState({
            answerHidden: true,
            currentQuestionIndex: this.state.currentQuestionIndex + 1 > arrSize - 1 ? this.state.currentQuestionIndex : this.state.currentQuestionIndex + 1
        })
    }

    previousQuestion = () => {
        this.setState({
            answerHidden: true,
            currentQuestionIndex: this.state.currentQuestionIndex - 1 <= 0 ? 0 : this.state.currentQuestionIndex - 1
        })
    }

    randomQuestion = (arrSize) => {
        this.setState({
            answerHidden: true,
            currentQuestionIndex: Math.floor(Math.random() * arrSize)
        });
    }

    answerDiv = (questions) => {
        if (this.state.answerHidden) return null;

        return (
            <div className='viewQuestionContentAnswerDiv'>
                {this.displayContents('answers', questions)}
            </div>
        )
    }

    displayTags = (questions) => {
        if (this.state.currentQuestionIndex < 0 || this.state.currentQuestionIndex >= questions.length) return;
        const tags = questions[this.state.currentQuestionIndex].tags;

        if (tags) {
            return tags.map(tag => {
                const objTag = findByField(this.props.categories, tag, 'key');
                return (objTag ? <Tag key={tag}>{objTag.name}</Tag> : '')
            })
        }
    }

    displayContents = (type, questions) => {
        if (this.state.currentQuestionIndex < 0 || this.state.currentQuestionIndex >= questions.length) { console.log('Out of bounds'); return 'Out of bounds' };

        if ('questions' !== type && 'answers' !== type) {
            console.log('Wrong type');
            return 'Wrong type';
        } else {
            type = type === 'questions' ? 'questionArray' : 'answerArray';
        }

        const contents = questions[this.state.currentQuestionIndex][type];

        return contents.map((content, index) => {
            if (content.type === 'title') {
                return <div className='titleView' key={index}>{content.value}</div>
            } else if (content.type === 'description') {
                return <div className='descriptionView' key={index}>
                    <pre>{content.value}</pre>
                </div>
            } else if (content.type === 'code') {
                return <div className='descriptionView' key={index}>
                    <SyntaxHighlighter showLineNumbers={true} wrapLines={false} language={content.language} style={docco}>
                        {content.value}
                    </SyntaxHighlighter>
                </div>
            } else if (content.type === 'image') {
                return <div className='descriptionView' key={index}>
                    <img className='imgViewImg' src={content.value} alt={index} />
                </div>
            }
            return null;
        })
    }

    editQuestion = (question) => {
        this.props.history.push(`/question/addModify/${question.key}`);
    }

    jumpToQuestion = () => {
        this.setState({
            jumpModalVisible: true,
            modalIndex: this.state.currentQuestionIndex
        })
    }

    onJumpOk = () => {
        this.setState({
            currentQuestionIndex: this.state.modalIndex,
            answerHidden: true,
            jumpModalVisible: false
        })
    }

    onJumpCancel =() =>{
        this.setState({jumpModalVisible: false})
    }

    jumpModalIdxChange = (value) => {
        this.setState({
            modalIndex: value
        })
    }

    onJumpPrevious10 = () => {
        this.setState({
            currentQuestionIndex: this.state.currentQuestionIndex - 10 > 0 ? this.state.currentQuestionIndex - 10 : 0,
            answerHidden: true,
            jumpModalVisible: false
        })
    }

    render() {
        let localQuestions = this.props.questions;

        if (this.state.selectedType) {
            localQuestions = this.props.questions.filter(question => question.type === this.state.selectedType)
        }

        if (this.state.selectedCategories.length > 0) {
            localQuestions = this.props.questions.filter(question => !question.tags ? false : question.tags.some(tag => this.state.selectedCategories.includes(tag)))
        }

        return (
            <div className='rootViewQuestion'>
                <div className='rootViewQuestionFiltersDiv'>
                    {/* <Select size='large' value={this.state.selectedType} style={{ width: 200 }} onChange={this.handleTypeChange}>
                        {this.props.types.map(type => <Option value={type.id} key={type.id}>{type.label}</Option>)}
                    </Select> */}
                    {/* <Select mode="multiple" size='large' value={this.state.selectedCategories} style={{ width: 600 }} onChange={this.handleCategoryChange}>
                        {this.props.categories.map(category => <Option key={category.key} value={category.key}>{category.name}</Option>)}
                    </Select> */}

                    <Button onClick={this.props.returnCategorySelection}>Return</Button>
                    <Button onClick={this.jumpToQuestion}>{this.state.currentQuestionIndex}{' / '}{localQuestions.length - 1}</Button>
                    <Button onClick={() => this.editQuestion(localQuestions[this.state.currentQuestionIndex])}>Edit</Button>
                </div>
                <div className='viewQuestionContentQuestionDiv'>
                    {this.displayTags(localQuestions)}
                    {this.displayContents('questions', localQuestions)}
                </div>
                {this.answerDiv(localQuestions)}
                <div className='rootViewQuestionActionsDiv'>
                    <ButtonGroup>
                        <Button type="primary" icon="step-backward"
                            size={'large'}
                            disabled={this.state.currentQuestionIndex <= 0}
                            onClick={this.previousQuestion}
                        >{this.props.isDesktop ? 'Previous' : ''}</Button>
                        <Button type="primary" icon="close"
                            size={'large'}
                            onClick={this.addDescription}
                        >{this.props.isDesktop ? 'Wrong' : ''}</Button>
                        <Button type="primary" icon="retweet"
                            size={'large'}
                            onClick={()=>this.randomQuestion(localQuestions.length)}
                        >{this.props.isDesktop ? 'Random Question' : ''}</Button>
                        <Button type="primary" icon={this.state.answerHidden ? 'folder-open' : 'folder'}
                            size={'large'}
                            onClick={this.displayHideAnswer}
                        >{this.props.isDesktop ? (this.state.answerHidden ? 'Display Answer' : 'Hide Answer') : ''}</Button>
                        <Button type="primary" icon="check"
                            size={'large'}
                            onClick={this.addImage}
                        >{this.props.isDesktop ? 'Correct' : ''}</Button>
                        <Button type="primary" icon="step-forward"
                            size={'large'}
                            disabled={this.state.currentQuestionIndex >= localQuestions.length - 1}
                            onClick={() => this.nextQuestion(localQuestions.length)}
                        >{this.props.isDesktop ? 'Next' : ''}</Button>
                    </ButtonGroup>
                </div>

                <Modal 
                    title="Jump to Question"
                    visible={this.state.jumpModalVisible}
                    onOk={this.onJumpOk}
                    onCancel={this.onJumpCancel}
                    footer={[
                        <Button key="previous10" type="primary" onClick={this.onJumpPrevious10}>
                            Previous 10
                        </Button>,
                        <Button key="back" onClick={this.onJumpCancel}>
                            Return
                        </Button>,
                        <Button key="submit" type="primary" onClick={this.onJumpOk}>
                            Submit
                        </Button>
                    ]}>
                        <InputNumber value={this.state.modalIndex} min={0} max={localQuestions.length - 1} onChange={this.jumpModalIdxChange} />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    types: state.categoryReducer.types,
    categories: state.categoryReducer.categories,
    questions: state.questionReducer.questions,
    isDesktop: state.commonReducer.isDesktop
});

export default connect(mapStateToProps, null)(withRouter(ViewQuestion));