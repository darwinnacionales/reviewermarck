import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Select, Table, Button, Tag, Popconfirm } from 'antd';
import Column from 'antd/lib/table/Column';
import './ManageQuestions.css';

import { findByField, deleteFile } from '../../../Common/util';
import fire from '../../../fire';

const { Option } = Select;

class ManageQuestions extends Component {
    state = {
        selectedType: '',
        selectedCategory: [],
    }

    addQuestion = () => {
        this.props.history.push('/question/addModify/add');
    }

    handleTypeChange = (value) => {
        this.setState({
            selectedType: value
        })
    }

    handleCategoryChange = (value) => {
        this.setState({
            selectedCategory: value
        })
    }

    deleteQuestion = (data) => {
        const questionRef = fire.database().ref(`questions/${data.key}`);

        console.log(data);

        this.deleteImages(data.questionArray, data.answerArray);

        questionRef.remove();
    }

    deleteImages = (questions, answers) => {
        questions.forEach(questionItem => {
            if(questionItem.type === 'image' && questionItem.value) {
                const link = questionItem.value;

                if (link.indexOf('/o/') > 0 && link.indexOf('?alt') > 0) {
                    const startIdx = link.indexOf("/o/");
                    const endIdx = link.indexOf("?alt");
                    const imgFile = link.substring(startIdx + 3, endIdx);

                    deleteFile(imgFile);
                }
            }
        });

        answers.forEach(answerItem => {
            if(answerItem.type === 'image' && answerItem.value) {
                const link = answerItem.value;

                if (link.indexOf('/o/') > 0 && link.indexOf('?alt') > 0) {
                    const startIdx = link.indexOf("/o/");
                    const endIdx = link.indexOf("?alt");
                    const imgFile = link.substring(startIdx + 3, endIdx);

                    deleteFile(imgFile);
                }
            }
        });
    }

    render() {
        let localQuestions = this.props.questions;

        if(this.state.selectedType) {
            localQuestions = this.props.questions.filter(question => question.type === this.state.selectedType)
        }

        if(this.state.selectedCategory.length > 0) {
            localQuestions = this.props.questions.filter(question => !question.tags ? false : question.tags.some(tag => this.state.selectedCategory.includes(tag) ))
        }

        return (<div className='rootManageQuestions'>
            <div className='filterManageQuestionsDiv'>
                <Select allowClear size='large' value={this.state.selectedType} style={{ width: 200 }} onChange={this.handleTypeChange}>
                    {this.props.types.map(type => <Option value={type.id} key={type.id}>{type.label}</Option>)}
                </Select>
                <Select mode="tags" size='large' value={this.state.selectedCategory} style={{ width: 600 }} onChange={this.handleCategoryChange}>
                    {this.props.categories.map(category => <Option key={category.key} value={category.key}>{category.name}</Option>)}
                </Select>
            </div>
            <div className='tableManageQuestionsDiv'>
                <Table title={() => <div>Questions List <Button style={{ float: 'right' }} onClick={this.addQuestion}>Add</Button></div>}
                    dataSource={localQuestions}
                    pagination={{ pageSize: 10 }}
                    scroll={{ y: 500 }} >
                    <Column title="Type" key='type'
                        render={(text, record) => !text.type ? '' : findByField(this.props.types, text.type, 'id').label} />
                    <Column title="Question" key='question'
                        render={(text, record) => !text.questionArray ? '' : text.questionArray.map(q => q.type === 'title' || q.type === 'description' ? q.value + ' ' : '')} />
                    <Column title="Answer" key='answer'
                        render={(text, record) => !text.answerArray ? '' : text.answerArray.map(a => a.type === 'title' || a.type === 'description' ? a.value + ' ' : '')} />
                    <Column title="Tags" key='tags'
                        render={(text, record) => !text.tags ? '' : text.tags.map(tag => {
                            const objTag = findByField(this.props.categories, tag, 'key');
                            return (objTag ? <Tag key={tag}>{objTag.name}</Tag> : '')
                        })} />
                    <Column title="Action" key="action" width={250}
                        render={(text, record) => {
                            return (
                                <span>
                                    <Button.Group>
                                        <Button icon="edit" onClick={() => this.props.history.push(`/question/addModify/${text.key}`)}>{this.props.isDesktop? 'Edit' : ''}</Button>
                                        <Popconfirm
                                            title="Are you sure delete this question?"
                                            onConfirm={() => this.deleteQuestion(text)}
                                            okText="Yes"
                                            cancelText="No">
                                            <Button type="danger" icon="delete">{this.props.isDesktop ? 'Delete' : ''}</Button>
                                        </Popconfirm>
                                    </Button.Group>
                                </span>
                            )
                        }} />

                </Table>
            </div>
        </div>)
    }
}

const mapStateToProps = state => ({
    types: state.categoryReducer.types,
    categories: state.categoryReducer.categories,
    questions: state.questionReducer.questions,
    isDesktop: state.commonReducer.isDesktop
});

export default connect(mapStateToProps, null)(ManageQuestions);