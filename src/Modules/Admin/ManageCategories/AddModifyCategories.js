import React, { Component } from 'react';
import { Modal, Form, Input } from 'antd';
import { connect } from 'react-redux';
import { addCategory, updateCategory } from '../../../Redux/actions/categoryActions';

class AddModifyCategories extends Component {
    state = {
        mode: 'create',
        display: {
            title: 'Create a New Category',
            confirmBtn: 'Create'
        },
        values: {
            key: '',
            name: '',
            description: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mode === 'create') {
            this.setState({
                ...this.state,
                mode: nextProps.mode,
                display: {
                    title: 'Create a New Category',
                    confirmBtn: 'Create'
                },
                values: {
                    key: '',
                    name: '',
                    description: ''
                }
            });
        } else {
            this.setState({
                ...this.state,
                mode: nextProps.mode,
                display: {
                    title: 'Update Category',
                    confirmBtn: 'Update'
                },
                values: {
                    key: nextProps.record.key,
                    name: nextProps.record.name,
                    description: nextProps.record.description
                }
            });
        }
    }

    onCancel = () => {
        this.props.toggleModal();
    }

    onCreate = () => {
        if(this.state.mode === 'create') {
            this.props.addCategory(this.state.values);
        } else {
            this.props.updateCategory(this.state.values);
        }
        this.props.toggleModal();
    }

    onFieldChange = (e) => {
        this.setState({
            ...this.state,
            values: {
                ...this.state.values,
                [e.target.name]: e.target.value
            }
        })
    }

    render() {
        return (
            <Modal
                visible={this.props.visible}
                title={this.state.display.title}
                okText={this.state.display.confirmBtn}
                onCancel={this.onCancel}
                onOk={this.onCreate}
            >
                <Form layout="vertical">
                    <Form.Item label="Name">
                        <Input name='name' onChange={this.onFieldChange} value={this.state.values.name} />
                    </Form.Item>
                    <Form.Item label="Description">
                        <Input name='description' onChange={this.onFieldChange} value={this.state.values.description} type="textarea" />
                    </Form.Item>
                </Form>
            </Modal>
        )
    }
}

export default connect(null, { addCategory, updateCategory })(AddModifyCategories);