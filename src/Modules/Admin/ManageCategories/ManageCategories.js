import React, { Component } from 'react';
import { Table, Button, Popconfirm } from 'antd';
import { connect } from 'react-redux';
import './ManageCategories.css';
import AddModifyCategories from './AddModifyCategories';
import Column from 'antd/lib/table/Column';

import { updateCategory, deleteCategory } from '../../../Redux/actions/categoryActions';

const typeColumns = [
    {
        title: 'ID',
        dataIndex: 'id',
        width: 30
    },
    {
        title: 'Label',
        dataIndex: 'label',
        width: 50
    }
]

class ManageCategories extends Component {
    state = {
        isModalVisible: false,
        modalMode: 'create',
        toEdit: null
    }

    toggleModal = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible
        })
    }

    deleteCategory = (recordToDelete) => {
        this.props.deleteCategory(recordToDelete);
    }

    editCategory = (recordToEdit) => {
        this.setState({
            isModalVisible: true,
            modalMode: 'edit',
            toEdit: recordToEdit
        })
    }

    addCategory = () => {
        this.setState({
            isModalVisible: true,
            modalMode: 'create',
            toEdit: null
        })
    }

    render() {
        return (<div className='manageCategoriesContainer'>
            <div className='manageCategoriesLeft'>
                <Table
                    title={() => <div>Types</div>}
                    columns={typeColumns}
                    rowKey='id'
                    dataSource={this.props.types}
                    pagination={{ pageSize: 10 }}
                    scroll={{ y: 500 }} />
            </div>
            <div className='manageCategoriesRight'>
                <Table
                    title={() => <div>Categories <Button style={{ float: 'right' }} onClick={this.addCategory}>Add</Button></div>}
                    dataSource={this.props.categories}
                    pagination={{ pageSize: 10 }}
                    rowKey={'key'}
                    scroll={{ y: 500 }} >
                    <Column title="Name" dataIndex="name" />
                    <Column title="Description" dataIndex="description"  />
                    <Column title="Action"
                        render={(text, record) => (
                            <span>
                                <Button.Group>
                                    <Button icon="edit" onClick={() => this.editCategory(record)}>{this.props.isDesktop ? 'Edit' : ''}</Button>
                                    <Popconfirm
                                            title="Are you sure delete this category?"
                                            onConfirm={() => this.deleteCategory(record)}
                                            okText="Yes"
                                            cancelText="No">
                                        <Button type="danger" icon="delete">{this.props.isDesktop ? 'Delete' : ''}</Button>
                                    </Popconfirm>
                                </Button.Group>
                            </span>
                        )} />
                </Table>
            </div>
            <AddModifyCategories visible={this.state.isModalVisible} toggleModal={this.toggleModal} mode={this.state.modalMode} record={this.state.toEdit} />
        </div>)
    }
}

const mapStateToProps = state => ({
    types: state.categoryReducer.types,
    categories: state.categoryReducer.categories,
    isDesktop: state.commonReducer.isDesktop
});

export default connect(mapStateToProps, { updateCategory, deleteCategory })(ManageCategories);