import React, { Component } from 'react';
import './SideNavigation.css';
import { Menu, Icon } from 'antd';
import Sider from 'antd/lib/layout/Sider';

import logo from '../../res/logo.png';
import SubMenu from 'antd/lib/menu/SubMenu';
import { withRouter } from 'react-router-dom'

class SideNavigation extends Component {
    onSelection = (selected) => {
        this.props.history.push('/' + selected);
    }
    
    render() {
        return (
        <Sider
            collapsible
            collapsedWidth={0}
            collapsed={this.props.collapsed}
            style={{
                overflowY: 'auto',
                overflowX: 'hidden',
                height: '100vh',
                position: 'fixed',
                left: 0,
                selectable: 'false',
                zIndex: 200
            }}
            >
            <div className="logo">
                <img alt='img of logo' src={logo} />
            </div>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                <Menu.Item key="1" onClick={() => this.onSelection('dashboard')}>
                    <Icon type="pie-chart" />
                    <span className="nav-text">Dashboard</span>
                </Menu.Item>
                <Menu.Item key="2" onClick={() => this.onSelection('quiz')}>
                    <Icon type="thunderbolt" />
                    <span className="nav-text">Quiz</span>
                </Menu.Item>
                    <SubMenu
                        key="sub1"
                        title={
                            <span>
                                <Icon type="codepen" />
                                <span>Coding</span>
                            </span>
                        }
                    >
                        <Menu.Item key="3">
                            <Icon type="windows" />
                            <span className="nav-text">System Design</span>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Icon type="linkedin" />
                            <span className="nav-text">OO Design</span>
                        </Menu.Item>
                        <Menu.Item key="5">
                            <Icon type="google" />
                            <span className="nav-text">Operating System</span>
                        </Menu.Item>
                        <Menu.Item key="6">
                            <Icon type="facebook" />
                            <span className="nav-text">Algorithms</span>
                        </Menu.Item>
                        <Menu.Item key="7">
                            <Icon type="instagram" />
                            <span className="nav-text">Database</span>
                        </Menu.Item>
                        <Menu.Item key="8">
                            <Icon type="amazon" />
                            <span className="nav-text">Shell</span>
                        </Menu.Item>
                    </SubMenu>
                <Menu.Item key="9" onClick={() => this.onSelection('manage-categories')}>
                    <Icon type="container" />
                    <span className="nav-text">Manage Categories</span>
                </Menu.Item>
                <Menu.Item key="10" onClick={() => this.onSelection('manage-questions')}>
                    <Icon type="database" />
                    <span className="nav-text">Manage Questions</span>
                </Menu.Item>
            </Menu>
        </Sider>)
    }
}

export default withRouter(SideNavigation);