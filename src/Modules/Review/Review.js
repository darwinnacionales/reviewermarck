import React, { Component } from 'react';
import './Review.css';
import { withRouter } from 'react-router-dom'
import { Button, Table } from 'antd';
import { connect } from 'react-redux';
import ViewQuestion from '../Question/ViewQuestion';

const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      width: 300
    }
  ];

class Review extends Component {
    state = {
        targetKeys: [],
        selectedKeys: [],
        display: 'pre-quiz'
    }

    startQuiz = () => {
        this.setState({
            display: 'quiz'
        })
    }

    returnCategorySelection = () => {
        this.setState({
            display: 'pre-quiz'
        })
    }

    handleChange = (nextTargetKeys, direction, moveKeys) => {
        this.setState({ targetKeys: nextTargetKeys });
    };

    handleSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
        this.setState({ selectedKeys: [...sourceSelectedKeys, ...targetSelectedKeys] });
    };

    render() {
        let toDisplay;

        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
              this.setState({targetKeys: selectedRowKeys})
            },
          };

        if (this.state.display === 'pre-quiz') {
            toDisplay = <div className='reviewRoot'>
                <div className='reviewTransfer'>
                    <Table rowSelection={rowSelection} columns={columns} dataSource={this.props.categories} />
                </div>
                <div className='reviewAction'><Button onClick={this.startQuiz}>Start Quiz</Button></div>
            </div>
        } else {
            toDisplay = <ViewQuestion selectedCategoriesFromPreQuiz={this.state.targetKeys} returnCategorySelection={this.returnCategorySelection} />
        }

        return toDisplay;
    }
}

const mapStateToProps = state => ({
    categories: state.categoryReducer.categories,
    screenWidth: state.commonReducer.screenWidth,
    isDesktop: state.commonReducer.isDesktop
});


export default connect(mapStateToProps, null)(withRouter(Review));