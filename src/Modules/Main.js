import { Layout, Button } from 'antd';
import './Main.css';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import SideNavigation from './Common/SideNavigation';
import { Switch, Route } from 'react-router-dom';
import Dashboard from './Dashboard/Dashboard';
import Review from './Review/Review';
import ManageCategories from './Admin/ManageCategories/ManageCategories';
import ManageQuestions from './Admin/ManageQuestions/ManageQuestions';
import {loadCategories} from '../Redux/actions/categoryActions';
import AddModifyQuestion from './Question/AddModifyQuestion';

import { loadQuestions } from '../Redux/actions/questionActions';
import { screenSizeChanged } from '../Redux/actions/commonActions';

const { Header, Content } = Layout;

class Main extends Component {
    state = {
        sideNavCollapsed: true
    }

    componentDidMount() {
        this.updatePredicate();
        window.addEventListener("resize", this.updatePredicate);
        
        this.initCategories();
        this.props.loadQuestions();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updatePredicate);
    }

    updatePredicate() {
        try {
            if(window) this.props.screenSizeChanged(window.innerWidth);
        } catch (e) { }
    }

    initCategories = () => {
        this.props.loadCategories();
    }

    toggleSideNav = () => {
        this.setState({
            sideNavCollapsed: !this.state.sideNavCollapsed
        })
    }

    render() {
        return (
            <Layout className="mainLayout">
                <SideNavigation collapsed={this.state.sideNavCollapsed} />
                <Layout style={{ marginLeft: this.state.sideNavCollapsed ? 0 : 200 }}>
                    <Header style={{ padding: 0, zIndex: 100, position: 'fixed', width: '100%', color: 'white', fontSize: '1.25em' }} 
                        >
                        
                        <Button //style={{float: 'right', marginTop: '15px', marginRight: this.state.sideNavCollapsed ? 15 : 215}}
                            onClick={this.toggleSideNav}
                            style={{marginRight: '15px', marginLeft: '15px'}} 
                            shape="circle" icon="menu" />Marck It
                    </Header>
                    <Content className="mainContent">
                        <Switch>
                            <Route exact path='/' component={Dashboard}/>
                            <Route exact path='/dashboard' component={Dashboard}/>
                            <Route exact path='/quiz' component={Review}/>
                            <Route exact path='/manage-categories' component={ManageCategories}/>
                            <Route exact path='/manage-questions' component={ManageQuestions}/>
                            <Route exact path='/question/addModify/:id' component={AddModifyQuestion}/>
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default (connect(null, { loadCategories, screenSizeChanged, loadQuestions })(Main));