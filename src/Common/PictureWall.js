import React, { Component } from 'react';
import { Upload, Icon, message } from 'antd';
import fire from '../fire';
import { generateRandomString, deleteFile } from './util';

class PictureWall extends Component {
    state = {
        imageUrl: '',
        loading: false,
    };

    componentDidMount() {
        this.setState({
            imageUrl: this.props.imageUrl
        })
    }

    getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }

    beforeUpload = (file) => {
        const isJPGorPNG = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJPGorPNG) {
            message.error('You can only upload JPG or PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJPGorPNG && isLt2M;
    }

    uploadPhoto = (file) => {
        this.setState({ loading: true });
        const fileName = generateRandomString();
        const storageRef = fire.storage().ref();
        const mountainsRef = storageRef.child(fileName);

        if(this.state.imageUrl && this.state.imageUrl.indexOf('/o/') > 0 && this.state.imageUrl.indexOf('?alt') > 0) {
            const startIdx = this.state.imageUrl.indexOf("/o/");
            const endIdx = this.state.imageUrl.indexOf("?alt");
            const imgFile = this.state.imageUrl.substring(startIdx + 3, endIdx);
            
            deleteFile(imgFile);
        }

        mountainsRef.put(file).then(snapshot => {
            mountainsRef.getDownloadURL().then(url => {
                this.setState({
                    imageUrl: url,
                    loading: false,
                })

                this.props.onChangeImage(this.props.name, url);
            }).catch(error => {
                console.log(error);
                this.setState({
                    loading: false,
                })
                message.error('Upload Failed! Please try again later.');
                // A full list of error codes is available at
                // https://firebase.google.com/docs/storage/web/handle-errors
                // switch (error.code) {
                //   case 'storage/object-not-found':
                //     // File doesn't exist
                //     break;
              
                //   case 'storage/unauthorized':
                //     // User doesn't have permission to access the object
                //     break;
              
                //   case 'storage/canceled':
                //     // User canceled the upload
                //     break;
              
                //   ...
              
                //   case 'storage/unknown':
                //     // Unknown error occurred, inspect the server response
                //     break;
                // }
              });
        }).catch(error => {
            console.log(error);
            this.setState({
                loading: false,
            })
            message.error('Upload Failed! Please try again later.');
        });
    }

    render() {
        const uploadButton = (
            <div>
                <Icon type={this.state.loading ? 'loading' : 'plus'} />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        const { imageUrl } = this.state;
        return (
            <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action={this.uploadPhoto}
                beforeUpload={this.beforeUpload}
            >
                {imageUrl ? <img style={{maxWidth: '100%'}} src={imageUrl} alt="avatar" /> : uploadButton}
            </Upload>
        );
    }
}

export default PictureWall;