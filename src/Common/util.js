import fire from '../fire';

export const findByField = (objects, keyToFind, field) => {
    return objects.find(obj => obj[field] === keyToFind);
}

export const generateRandomString = () => {
    const dt = new Date();
    const randString = dt.getTime() + '-' + Math.random().toString(36).substring(2, 15) + '-' + Math.random().toString(36).substring(2, 15);

    return randString;
}

export const deleteFile = (imgFile) => {
    const storageRef = fire.storage().ref();

    const imgRef = storageRef.child(imgFile);

    // Delete the file
    imgRef.delete().then(function () {

    }).catch(function (error) {
        console.error(error);
    });
}