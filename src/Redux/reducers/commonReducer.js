import { SCREEN_SIZE_CHANGED } from "../actions/types";

const initialState = {
    isDesktop: true,
    screenWidth: 1000
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SCREEN_SIZE_CHANGED:
            return {
                ...state,
                isDesktop: action.payload.isDesktop,
                screenWidth: action.payload.screenWidth
            }
        default:
            return state;
    }
}