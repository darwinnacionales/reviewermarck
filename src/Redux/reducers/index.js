import { combineReducers } from 'redux';

import categoryReducer from './categoryReducer';
import questionReducer from './questionReducer';
import commonReducer from './commonReducer';

export default combineReducers({
    categoryReducer,
    questionReducer,
    commonReducer,
});