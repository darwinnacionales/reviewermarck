import { LOAD_CATEGORIES } from "../actions/types";

const initialState = {
    types: [{
        id: 'quiz',
        label: 'Quiz'
    },{
        id: 'system_design',
        label: 'System Design'
    },{
        id: 'oo_design',
        label: 'OO Design'
    },{
        id: 'operating_system',
        label: 'Operating System'
    },{
        id: 'algorithms',
        label: 'Algorithms'
    },{
        id: 'database',
        label: 'Database'
    },{
        id: 'shell',
        label: 'Shell'
    }],
    categories: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOAD_CATEGORIES:
            return {
                ...state,
                categories: action.payload
            }
        default:
            return state;
    }
}