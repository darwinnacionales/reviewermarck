import { LOAD_CATEGORIES } from "./types";
import fire from '../../fire';

export const loadCategories = () => dispatch => {
    const categoriesRef = fire.database().ref('categories/');

    categoriesRef.on('value', snapshot => {
        const categories = [];

        Object.keys(snapshot.val()).forEach(function(key) {
            categories.push({
                key: key,
                ...snapshot.val()[key]
            });
        });

        dispatch({
            type: LOAD_CATEGORIES,
            payload: categories
        })
    });
}

export const addCategory = (data) => dispatch => {
    const categoriesRef = fire.database().ref('categories/');
    let newCategoryRef = categoriesRef.push();
    newCategoryRef.set({
        name: data.name,
        description: data.description
    });
}

export const updateCategory = (data) => dispatch => {
    const categoryRef = fire.database().ref(`categories/${data.key}`);
    categoryRef.update({
        name: data.name,
        description: data.description
    })
}

export const deleteCategory = (data) => dispatch => {
    const categoryRef = fire.database().ref(`categories/${data.key}`);
    categoryRef.remove();
}