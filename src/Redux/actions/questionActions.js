import { LOAD_QUESTIONS } from "./types";
import fire from '../../fire';

export const loadQuestions = () => dispatch => {
    const questionsRef = fire.database().ref('questions/');

    questionsRef.on('value', snapshot => {
        const questions = [];

        if(snapshot.val()) {
            Object.keys(snapshot.val()).forEach(function(key) {
                questions.push({
                    key: key,
                    ...snapshot.val()[key]
                });
            });
    
            dispatch({
                type: LOAD_QUESTIONS,
                payload: questions
            })
        }
    });
}