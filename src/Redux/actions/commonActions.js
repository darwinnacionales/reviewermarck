import { SCREEN_SIZE_CHANGED } from "./types";

export const screenSizeChanged = (screenWidth) => dispatch => {
    dispatch({
        type: SCREEN_SIZE_CHANGED,
        payload: {
            isDesktop: screenWidth > 1000 ? true : false,
            screenWidth
        }
    })
}